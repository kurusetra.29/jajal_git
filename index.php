<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Function</title>
</head>
<body>
    <h1>Fungsi Tentukan Nilai</h1>
    <?php
    function tentukan_nilai($number)
    {
        //  kode disini
        if (85 <= $number && $number<= 100) {
            echo "Sangat Baik <br>"; 
          } elseif (70 <= $number && $number< 85) {
            echo "Baik <br>";
          } elseif (60 <= $number && $number< 70) {
            echo "Cukup <br>";
          } else {
            echo "Kurang <br>";
          } 
    }

    //TEST CASES
    echo tentukan_nilai(98);  //Sangat Baik
    echo tentukan_nilai(76);  //Baik
    echo tentukan_nilai(67); //Cukup
    echo tentukan_nilai(43); //Kurang

//    <?php
//kode di sini
    $tampung = '';
    function reverseString($kalimat){
        for ($i= strlen($kalimat) - 1; $i >= 0; $i--) {
            $huruf = substr($kalimat,$i,1);
            @$tampung .= $huruf;
        }
        return $tampung;
    }
    function ubah_huruf($string){
        $kata = reverseString($string);
        for ($i= strlen($kata) - 1; $i >= 0; $i--) {
            $asci = ord($kata[$i]);
            $ganti = chr($asci+1);
            @$huruf .= $ganti;
        }
        echo "$huruf <br>";
        //return $tampung;
    }

    // function ubah_huruf($string){
    //     $huruf = "";
    //     for ($i= strlen($string) - 1; $i >= 0; $i--) {
    //         $asci = ord($string[$i]);
    //         //echo $asci; echo "<br>";
    //         $ganti = chr($asci+1);
    //         //echo $ganti; echo "<br>";
    //         @$huruf .= $ganti;
    //     }
    //     echo "$huruf <br>";
    //     //return $tampung;
    
    // }

    // TEST CASES
     echo ubah_huruf('wow'); // xpx
     echo ubah_huruf('developer'); // efwfmpqfs
     echo ubah_huruf('laravel'); // mbsbwfm
     echo ubah_huruf('keren'); // lfsfo
     echo ubah_huruf('semangat'); // tfnbohbu
    ?>
</body>
</html>